<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));    }

    public function create(){
        return view('cast.create'); //untuk menuju ke form create/tambah data cast
    }

    public function store(Request $request) // fungsi tambah data ke tabel
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first(); // Berfungsi memanpilkan data berdasarkan $id yang terdapat pada parameter public funtion show($id) sintax sqlnya : SELECT * FROM post WHERE id = $id
        return view('cast.show', compact('cast'));
    }

    public function edit($id) // Berfungsi untuk menampilkan data berdasarkan $id yang terdapat pada parameter public funtion show($id) sintax sqlnya : SELECT * FROM post WHERE id = $id
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request) // Berfungsi untuk mengeksekusi edit ke dalam database
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function destroy($id) // Berfungsi untuk menghapus data
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
