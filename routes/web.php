<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'homeController@home' ); // Untuk menentukan mana yang akan ditampilkan ketika pertama kali memanggil localhost:port

Route::get('/register', 'authController@register' ); // Untuk menentukan link Form Register yang diatur oleh authController. Nama folder = Case sensitive
Route::post('/welcome', 'authController@welcome' ); // Untuk menentukan link Form Register yang diatur oleh authController. Nama folder = Case sensitive

Route::get('/data-table', function(){
    return view('table.data-table'); // table adalah nama folder dimana file data-table.blade.php berada
});

Route::get('/graph-table', function(){
    return view('table.graph-table'); // table adalah nama folder dimana file graph-table.blade.php berada
});

//CRUD CAST

Route::get('/cast','CastController@index');
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast/{cast_id}','CastController@show');
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
Route::delete('/cast/{cast_id}','CastController@destroy');
