@extends('layout.master')
@section('judul') <!-- samakan nama section (contohnya) judul dengan nama yang ada pada (contohnya) file master.blade.php di yield-->
    Halaman Form
@endsection
@section('isi')
    <h1>Form Register !</h1>
        <h2>Sign Up Form</h2>
        <!-- Form Dimulai -->
        <form action="welcome" method="post">
        @csrf <!-- @ csrf ini untuk pembuatan token -->
        <p>First name:</p>
        <input type="text" name="fname"><br>
        <p>Last name:</p>
        <input type="text" name="lname">
        <p>Gender:</p>
        <!-- Form Elemen Radio Button -->
        <input type="radio" name="gender" value="male">
        <label>Male</label><br>
        <input type="radio" name="gender" value="female">
        <label>Female</label><br>
        <input type="radio" name="gender" value="other">
        <label>Other</label><br>
        <!-- Form Elemen Select -->
        <p>Nationality:</p>
        <select name="nationality">
        <option value="-">-- Please Select -- </option>
        <option value="indonesian" selected>Indonesian</option>
        <option value="singapore">Singapore</option>
        <option value="us">United States</option>
        </select> 
        <!-- Form Elemen Checklist -->
        <p>Language Spoken:</p>
        <input type="checkbox" name="language1" value="indonesia">
        <label>Bahasa Indonesia</label><br>
        <input type="checkbox" name="language3" value="english">
        <label>English</label><br>
        <input type="checkbox" name="language3" value="other">
        <label>Other</label><br>
        <!-- Form Elemen Textarea -->
        <p>Bio:</p>
        <textarea name="bio" style="width:300px; height:150px;">
        </textarea> <br>
        <!-- Form Elemen Submit Button -->
        <input type="submit" value="Sign Up">
        <!-- Form Diakhiri -->
        </form>
@endsection
