@extends('layout.master')
@section('judul') <!-- samakan nama section (contohnya) judul dengan nama yang ada pada (contohnya) file master.blade.php di yield-->
    Halaman Detail Cast
@endsection
@section('isi')
    <h2>Show Cast {{$cast->id}}</h2>
    <h4>Nama Cast : {{$cast->nama}}</h4>
    <p>Umur Cast : {{$cast->umur}}</p>
    <p>Bio Cast : {{$cast->bio}}</p>
@endsection
